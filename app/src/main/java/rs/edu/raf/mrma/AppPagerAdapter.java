package rs.edu.raf.mrma;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import rs.edu.raf.mrma.tabs._1_add_expense.AddExpenseFragment;
import rs.edu.raf.mrma.tabs._2_expense_list.ExpenseOverviewFragment;
import rs.edu.raf.mrma.tabs._3_breakdown.BreakdownDonut;
import rs.edu.raf.mrma.tabs._3_breakdown.BreakdownFragment;
import rs.edu.raf.mrma.tabs._4_add_category.AddCategoryFragment;

public class AppPagerAdapter extends FragmentPagerAdapter {

    public AppPagerAdapter(FragmentManager fragmentManager, Context context) {
        super(fragmentManager);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: return AddExpenseFragment.newInstance();
            case 1: return ExpenseOverviewFragment.newInstance();
            case 2: return BreakdownFragment.newInstance();
            case 3: return AddCategoryFragment.newInstance();
            default: return null;
        }
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0: return "Add expense";
            case 1: return "Overview";
            case 2: return "Breakdown";
            case 3: return "Add category";
            default: return null;
        }
    }
}
