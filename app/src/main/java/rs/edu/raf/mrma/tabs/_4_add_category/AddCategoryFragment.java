package rs.edu.raf.mrma.tabs._4_add_category;

import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rs.edu.raf.mrma.R;
import rs.edu.raf.mrma.model.AppViewModel;
import rs.edu.raf.mrma.model.Category;
import rs.edu.raf.mrma.tabs._1_add_expense.AddExpenseFragment;
import rs.edu.raf.mrma.tabs._4_add_category.rv.CategoriesRVAdapter;

public class AddCategoryFragment extends Fragment {
    private static final String TAG = "ADD_CATEGORY_FRAGMENT";

    public static AddCategoryFragment newInstance() {
        AddCategoryFragment fragment = new AddCategoryFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    private AppViewModel vm;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.add_category, container, false);
        ButterKnife.bind(this, v);
        Bundle args = getArguments();

        vm = ViewModelProviders.of(getActivity()).get(AppViewModel.class);

        init();

        return v;
    }

    private void init() {
        categoriesRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        CategoriesRVAdapter adapter = new CategoriesRVAdapter();
        categoriesRecyclerView.setAdapter(adapter);


        vm.getCategoriesLiveData().observe(this,
                c -> adapter.setCategories(c));


        categoriesRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                int margin = 20;
                outRect.top = outRect.bottom = outRect.left = outRect.right = margin;
            }
        });

    }

    @BindView(R.id.add_category_recycler)
    RecyclerView categoriesRecyclerView;

    @BindView(R.id.add_category_name)
    EditText categoryNameEditText;

    @OnClick(R.id.add_category_confirm)
    public void handleAddClicked() {
        Category category = new Category(categoryNameEditText.getText().toString().trim());
        boolean success = vm.addCategory(category);
        if (success) {
            categoryNameEditText.getText().clear();
        } else {
            Toast.makeText(getActivity(), "Category already exists", Toast.LENGTH_LONG).show();
        }
    }

}
