package rs.edu.raf.mrma.tabs._2_expense_list;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import java.util.List;
import java.util.stream.Collectors;

import rs.edu.raf.mrma.model.Category;
import rs.edu.raf.mrma.model.Expense;

public class FilterViewModel extends ViewModel {

    private Filter filter;
    private MutableLiveData<Filter> filterLiveData;
    private LiveData<List<Expense>> filteredExpensesLiveData;

    public FilterViewModel() {
        filter = new Filter("", Category.NONE);
        filterLiveData = new MutableLiveData<>();
        filterLiveData.setValue(filter);
    }

    public MutableLiveData<Filter> getFilterLiveData() {
        return filterLiveData;
    }

    public void setName(String name) {
        filter = new Filter(name, filter.getCategory());
        filterLiveData.setValue(filter);
    }

    public void setCategory(Category category) {
        filter = new Filter(filter.getName(), category);
        filterLiveData.setValue(filter);
    }

    public LiveData<List<Expense>> getExpensesLiveData() {
        return filteredExpensesLiveData;
    }

    public void setExpensesLiveData(MutableLiveData<List<Expense>> expensesLiveData) {
        filteredExpensesLiveData = Transformations.switchMap(filterLiveData, filter ->
                Transformations.map(expensesLiveData, expenses -> {
                        return expenses.stream()
                                .filter(e -> e.getName().contains(filter.getName()))
                                .filter(e -> {
                                    if (filter.getCategory().equals(Category.NONE)) {
                                        return true;
                                    } else {
                                        Category expenseCategory = e.getCategory();
                                        Category filterCategory = filter.getCategory();
                                        return expenseCategory.equals(filterCategory);
                                    }
                                })
                                .collect(Collectors.toList());}
                )
        );

    }
}
