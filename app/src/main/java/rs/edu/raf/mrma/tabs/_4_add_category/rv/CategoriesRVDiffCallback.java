package rs.edu.raf.mrma.tabs._4_add_category.rv;

import androidx.recyclerview.widget.DiffUtil;

import java.util.List;

import rs.edu.raf.mrma.model.Category;

public class CategoriesRVDiffCallback extends DiffUtil.Callback {

    private List<Category> oldCategories;
    private List<Category> newCategories;

    public CategoriesRVDiffCallback(List<Category> oldCategories, List<Category> newCategories) {
        this.oldCategories = oldCategories;
        this.newCategories = newCategories;
    }


    @Override
    public int getOldListSize() {
        return oldCategories.size();
    }

    @Override
    public int getNewListSize() {
        return newCategories.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        try {
            Category oldCategory = oldCategories.get(oldItemPosition);
            Category newCategory = newCategories.get(newItemPosition);
            return oldCategory.equals(newCategory);
        } catch (IndexOutOfBoundsException e) {
            return false;
        }
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return false;
    }
}
