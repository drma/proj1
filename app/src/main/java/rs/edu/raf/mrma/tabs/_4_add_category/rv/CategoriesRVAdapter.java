package rs.edu.raf.mrma.tabs._4_add_category.rv;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import rs.edu.raf.mrma.R;
import rs.edu.raf.mrma.model.Category;

public class CategoriesRVAdapter extends RecyclerView.Adapter<CategoryViewHolder> {
    public CategoriesRVAdapter() {

    }


    private List<Category> categories = new ArrayList<>();

    public void setCategories(List<Category> newCategories) {
        CategoriesRVDiffCallback cb = new CategoriesRVDiffCallback(categories, newCategories);
        DiffUtil.DiffResult result = DiffUtil.calculateDiff(cb);
        categories.clear();
        categories.addAll(newCategories);
        result.dispatchUpdatesTo(this);
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.category_rv_item, parent, false);
        return new CategoryViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        holder.setContent(categories.get(position));
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

}
