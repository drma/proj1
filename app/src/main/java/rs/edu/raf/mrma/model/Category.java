package rs.edu.raf.mrma.model;

import java.util.UUID;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class Category {
    public static final Category NONE = new Category("<none>");

    private String name;

    public Category(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @NonNull
    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (!(obj instanceof Category)) {
            return false;
        } else {
            Category otherCategory = (Category) obj;
            return this.getName().equalsIgnoreCase(otherCategory.getName());
        }
    }
}
