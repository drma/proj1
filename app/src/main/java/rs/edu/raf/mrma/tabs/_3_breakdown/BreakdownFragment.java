package rs.edu.raf.mrma.tabs._3_breakdown;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Pair;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rs.edu.raf.mrma.R;
import rs.edu.raf.mrma.model.AppViewModel;
import rs.edu.raf.mrma.model.Category;
import rs.edu.raf.mrma.model.Expense;
import rs.edu.raf.mrma.tabs._3_breakdown.rv.CategoryBreakdownRVAdapter;

public class BreakdownFragment extends Fragment {
    private static final String TAG = "BREAKDOWN_FRAGMENT";
    ;

    public static BreakdownFragment newInstance() {
        BreakdownFragment instance = new BreakdownFragment();
        Bundle args = new Bundle();
        instance.setArguments(args);
        return instance;
    }

    @BindView(R.id.breakdown_donut)
    BreakdownDonut donut;

    @BindView(R.id.category_breakdown_recycler)
    RecyclerView rv;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.breakdown, container, false);
        ButterKnife.bind(this, v);
        Bundle args = getArguments();

        init();

        return v;
    }

    private List<Category> categories;
    private List<Expense> expenses;
    private List<Pair<Category, Double>> categoriesAndTotals;
    private CategoryBreakdownRVAdapter adapter;

    private void init() {
        categoriesAndTotals = new ArrayList<>();
        categories = Collections.emptyList();
        expenses = Collections.emptyList();
        adapter = new CategoryBreakdownRVAdapter();

        AppViewModel vm = ViewModelProviders.of(getActivity()).get(AppViewModel.class);
        vm.getCategoriesLiveData().observe(this, this::handleCategoriesChange);
        vm.getExpensesLiveData().observe(this, this::handleExpensesChange);


        rv.setLayoutManager(new LinearLayoutManager(getContext()));
        rv.setAdapter(adapter);
    }

    private void handleCategoriesChange(List<Category> categories) {
        this.categories = categories;
        refresh();
    }

    private void handleExpensesChange(List<Expense> expenses) {
        this.expenses = expenses;
        refresh();
    }

    private void refresh() {
        categoriesAndTotals.clear();
        categories.stream()
                .map(c -> new Pair(c, totalForCategory(c)))
                .forEachOrdered(categoriesAndTotals::add);
        donut.setCategories(categoriesAndTotals);
        adapter.setCategories(categories);
    }

    private double totalForCategory(Category category) {
        return expenses.stream()
                .filter(e -> e.getCategory().equals(category))
                .mapToDouble(Expense::getAmount)
                .sum();
    }
}
