package rs.edu.raf.mrma.tabs._2_expense_list;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.function.Function;

import androidx.annotation.Nullable;

import butterknife.BindView;
import butterknife.ButterKnife;
import rs.edu.raf.mrma.R;
import rs.edu.raf.mrma.model.Expense;

public class ExpenseCardView extends LinearLayout {

    // region constructors

    public ExpenseCardView(Context context) {
        super(context);
        init(context, null);
    }

    public ExpenseCardView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public ExpenseCardView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public ExpenseCardView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    private Context context;

    private void init(Context context, AttributeSet attrs) {
        setOrientation(LinearLayout.VERTICAL);

        LayoutInflater.from(getContext()).inflate(R.layout.expense_card_view, this);
        ButterKnife.bind(this);
    }

    // endregion

    // region ui bindings

    @BindView(R.id.expense_card_name)
    TextView nameTextView;

    @BindView(R.id.expense_card_amount)
    TextView amountTextView;

    @BindView(R.id.expense_card_date)
    TextView dateTextView;

    @BindView(R.id.expense_card_category)
    TextView categoryTextView;

    @BindView(R.id.expense_card_remove_button)
    Button removeButton;

    // endregion

    private Expense expense;

    public void setData(Expense expense) {
        this.expense = expense;
        refreshUi();
    }

    public void setOnRemoveClick(ExpenseRecyclerAdapter.OnRemoveClicked removeHandler) {
        removeButton.setOnClickListener((_v) -> {
            removeHandler.onRemoveClicked(expense);
        });
    }

    private void refreshUi() {
        nameTextView.setText(expense.getName());
        amountTextView.setText(Double.toString(expense.getAmount()));
        dateTextView.setText(expense.getDate().toString());
        categoryTextView.setText(expense.getCategory().getName());
    }

}
