package rs.edu.raf.mrma.model;

import android.widget.EditText;

import androidx.annotation.Nullable;

import java.util.Date;
import java.util.UUID;

public class Expense {

    private String id;
    private String name;
    private double amount;
    private Category category;
    private Date date;

    public Expense(String name, double amount, Category category) {
        this(name, amount, category, new Date());
    }

    public Expense(String name, double amount, Category category, Date date) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.amount = amount;
        this.category = category;
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getAmount() {
        return amount;
    }

    public Category getCategory() {
        return category;
    }

    public Date getDate() {
        return date;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (!(obj instanceof Expense)) {
            return false;
        } else {
            Expense otherExpense = (Expense) obj;
            return getId().equals(otherExpense.getId());
        }
    }
}
