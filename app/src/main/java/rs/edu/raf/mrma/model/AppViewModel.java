package rs.edu.raf.mrma.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class AppViewModel extends ViewModel {

    private List<Expense> expenses;
    private List<Category> categories;

    // List of expenses
    private MutableLiveData<List<Expense>> expensesLiveData;
    private MutableLiveData<List<Category>> categoriesLiveData;

    public AppViewModel() {
        expenses = new ArrayList<>();
        expensesLiveData = new MutableLiveData<>();
        expensesLiveData.setValue(expenses);

        categories = Stream.of(
                new Category("Dumb shit I don't need"),
                new Category("Drugs"),
                new Category("Vidgergames"))
                .collect(Collectors.toList());
        categoriesLiveData = new MutableLiveData<>();
        categoriesLiveData.setValue(categories);
    }

    public MutableLiveData<List<Expense>> getExpensesLiveData() {
        return expensesLiveData;
    }

    public MutableLiveData<List<Category>> getCategoriesLiveData() {
        return categoriesLiveData;
    }

    public void addExpense(Expense expense) {
        expenses.add(expense);
        expensesLiveData.setValue(expenses);
    }

    public void removeExpense(Expense expense) {
        boolean listChanged = expenses.removeIf(e -> e.getId().equals(expense.getId()));
        if (listChanged) {
            expensesLiveData.setValue(expenses);
        }
    }

    public boolean addCategory(Category category) {
        if (categories.contains(category)) {
            return false;
        }
        categories.add(category);
        categoriesLiveData.setValue(categories);
        return true;
    }
}
