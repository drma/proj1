package rs.edu.raf.mrma.tabs._1_add_expense;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rs.edu.raf.mrma.R;
import rs.edu.raf.mrma.model.AppViewModel;
import rs.edu.raf.mrma.model.Category;
import rs.edu.raf.mrma.model.Expense;

public class AddExpenseFragment extends Fragment {
    private static final String TAG = "ADD_EXPENSE_FRAGMENT";

    public static AddExpenseFragment newInstance() {
        AddExpenseFragment fragment = new AddExpenseFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    // region UI bindings

    @BindView(R.id.add_expense_name)
    EditText expenseNameField;

    @BindView(R.id.add_expense_amount)
    EditText expenseAmountField;

    @BindView(R.id.add_expense_categories)
    Spinner categoriesSpinner;

    @OnClick(R.id.add_expense_confirm)
    void onConfirmButtonClicked() {
        String expenseName = expenseNameField.getText().toString();

        double expenseAmount;
        try {
            expenseAmount = Double.parseDouble(expenseAmountField.getText().toString());
        } catch (NumberFormatException e) {
            // TODO: toast se ne vidi ako korisnik ima tastaturu sa light temom
            Toast errorToast = Toast.makeText(getActivity(), R.string.invalid_amount, Toast.LENGTH_LONG);
            errorToast.show();
            return;
        }
        int selectedItemPosition = categoriesSpinner.getSelectedItemPosition();

        Category category = categories.get(selectedItemPosition);

        vm.addExpense(new Expense(expenseName, expenseAmount, category));
    }

    // endregion

    // region vm bindings

    private AppViewModel vm;
    private List<Category> categories;
    private ArrayAdapter<Category> spinnerAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.add_expense, container, false);
        ButterKnife.bind(this, v);
        Bundle args = getArguments();
        return v;
    }

    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initViewModel();
    }

    private void initViewModel() {
        vm = ViewModelProviders.of(getActivity()).get(AppViewModel.class);
        categories = new ArrayList<>();
        spinnerAdapter = new ArrayAdapter<Category>(getContext(), android.R.layout.simple_spinner_dropdown_item, categories);
        categoriesSpinner.setAdapter(spinnerAdapter);
        vm.getCategoriesLiveData().observe(getViewLifecycleOwner(), this::handleCategoriesChange);
    }

    private void handleCategoriesChange(List<Category> categories) {
        this.categories.clear();
        this.categories.addAll(categories);
        spinnerAdapter.notifyDataSetChanged();
    }

    // endregion

}
