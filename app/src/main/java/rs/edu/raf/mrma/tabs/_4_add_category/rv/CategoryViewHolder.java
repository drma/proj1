package rs.edu.raf.mrma.tabs._4_add_category.rv;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import rs.edu.raf.mrma.R;
import rs.edu.raf.mrma.model.Category;

public class CategoryViewHolder extends RecyclerView.ViewHolder {
    public CategoryViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @BindView(R.id.rv_category_name)
    TextView categoryNameTv;

    public void setContent(Category category) {
        categoryNameTv.setText(category.getName());
    }

}
