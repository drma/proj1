package rs.edu.raf.mrma.tabs._2_expense_list;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemSelected;
import butterknife.OnTextChanged;
import rs.edu.raf.mrma.R;
import rs.edu.raf.mrma.model.AppViewModel;
import rs.edu.raf.mrma.model.Category;
import rs.edu.raf.mrma.model.Expense;

public class ExpenseOverviewFragment extends Fragment {
    private static final String TAG = "EXPENSE_OVERVIEW_FRAGMENT";

    private AppViewModel vm;
    private FilterViewModel filterVm;

    public static ExpenseOverviewFragment newInstance() {
        ExpenseOverviewFragment fragment = new ExpenseOverviewFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.expense_overview, container, false);
        ButterKnife.bind(this, v);
        Bundle args = getArguments();

        initViewModels();
        initRecyclerView();
        initCategoriesSpinner();
        return v;
    }

    private void initViewModels() {
        vm = ViewModelProviders.of(getActivity()).get(AppViewModel.class);
        filterVm = ViewModelProviders.of(this).get(FilterViewModel.class);
        filterVm.setExpensesLiveData(vm.getExpensesLiveData());
    }

    private void initRecyclerView() {
        expensesRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        ExpenseRecyclerAdapter adapter = new ExpenseRecyclerAdapter();

        adapter.setOnRemoveClicked(vm::removeExpense);

        vm.getCategoriesLiveData().observe(this, adapter::setCategories);
        filterVm.getExpensesLiveData().observe(this, adapter::setExpenses);
        expensesRecyclerView.setAdapter(adapter);
    }

    private void initCategoriesSpinner() {
        List<Category> categories = new ArrayList<>();
        categories.add(Category.NONE);
        ArrayAdapter<Category> spinnerAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, categories);
        categorySpinner.setAdapter(spinnerAdapter);

        vm.getCategoriesLiveData().observe(getViewLifecycleOwner(), newCategories -> {
            categories.clear();
            categories.add(Category.NONE);
            categories.addAll(newCategories);
            spinnerAdapter.notifyDataSetChanged();
        });
    }

    // region UI bindings

    @BindView(R.id.filter_edit_text)
    EditText filterEditText;

    @OnTextChanged(R.id.filter_edit_text)
    void onFilterChanged(CharSequence text) {
        String filter = text.toString();
        filterVm.setName(filter);
    }

    @BindView(R.id.category_spinner)
    Spinner categorySpinner;

    @OnItemSelected(R.id.category_spinner)
    void onCategoryClick(int position) {
        if (position == 0) {
            filterVm.setCategory(Category.NONE);
        } else {
            Category selectedCategory = vm.getCategoriesLiveData().getValue().get(position-1);
            filterVm.setCategory(selectedCategory);
        }
    }

    @BindView(R.id.expense_overview_recycler_view)
    RecyclerView expensesRecyclerView;

    // endregion
}
