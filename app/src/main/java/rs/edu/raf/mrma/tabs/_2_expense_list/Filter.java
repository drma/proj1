package rs.edu.raf.mrma.tabs._2_expense_list;

import rs.edu.raf.mrma.model.Category;

public class Filter {
    private String name;
    private Category category;

    public Filter(String name, Category category) {
        this.name = name;
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public Category getCategory() {
        return category;
    }
}
