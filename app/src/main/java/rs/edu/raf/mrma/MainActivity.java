package rs.edu.raf.mrma;

import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        ViewPager pager = findViewById(R.id.app_pager);
        AppPagerAdapter adapter = new AppPagerAdapter(getSupportFragmentManager(), this);
        pager.setAdapter(adapter);
        TabLayout tabLayout = findViewById(R.id.app_tab_layout);
        tabLayout.setupWithViewPager(pager);
    }


}
