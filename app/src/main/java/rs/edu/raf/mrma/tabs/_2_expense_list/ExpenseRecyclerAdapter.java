package rs.edu.raf.mrma.tabs._2_expense_list;

import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import rs.edu.raf.mrma.model.Category;
import rs.edu.raf.mrma.model.Expense;

public class ExpenseRecyclerAdapter extends RecyclerView.Adapter<ExpenseRecyclerAdapter.Holder> {

    public static interface OnDetailClicked {
        public void detailClicked(Expense expense);
    }

    public static interface OnRemoveClicked {
        public void onRemoveClicked(Expense expense);
    }

    private OnRemoveClicked onRemoveClicked;
    private OnDetailClicked onDetailClicked;

    public void setOnRemoveClicked(OnRemoveClicked onRemoveClicked) {
        this.onRemoveClicked = onRemoveClicked;
    }

    public void setOnDetailClicked(OnDetailClicked onDetailClicked) {
        this.onDetailClicked = onDetailClicked;
    }

    // region vm binding
    private List<Category> categories;
    private List<Expense> expenses;

    public ExpenseRecyclerAdapter() {
        categories = new ArrayList<>();
        expenses = new ArrayList<>();
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public void setExpenses(List<Expense> expenses) {
        ExpensesDiffUtilCallback diffUtilCallback = new ExpensesDiffUtilCallback(this.expenses, expenses);
        DiffUtil.DiffResult result = DiffUtil.calculateDiff(diffUtilCallback);
        this.expenses.clear();
        this.expenses.addAll(expenses);
        result.dispatchUpdatesTo(this);

    }

    // endregion

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ExpenseCardView v = new ExpenseCardView(parent.getContext());
        v.setOnRemoveClick(this.onRemoveClicked);
        Holder holder = new Holder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.setContent(expenses.get(position));
    }

    @Override
    public int getItemCount() {
        return expenses.size();
    }

    public static class Holder extends RecyclerView.ViewHolder {

        private ExpenseCardView view;

        public Holder(@NonNull ExpenseCardView itemView) {
            super(itemView);
            view = itemView;
        }

        public void setContent(Expense expense) {
            view.setData(expense);
        }
    }


}
