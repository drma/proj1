package rs.edu.raf.mrma.tabs._3_breakdown.rv;

import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import rs.edu.raf.mrma.R;
import rs.edu.raf.mrma.model.Category;

public class CategoryBreakdownViewHolder extends RecyclerView.ViewHolder {

    public CategoryBreakdownViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @BindView(R.id.category_breakdown_card_color)
    TextView colorTv;

    @BindView(R.id.category_breakdown_card_name)
    TextView nameTv;

    public void setContent(Category category, String color) {
        colorTv.setBackgroundColor(Color.parseColor(color));
        nameTv.setText(category.getName());
    }
}
