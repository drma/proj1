package rs.edu.raf.mrma.tabs._2_expense_list;

import java.util.List;

import androidx.recyclerview.widget.DiffUtil;
import rs.edu.raf.mrma.model.Expense;

public class ExpensesDiffUtilCallback extends DiffUtil.Callback {

    private List<Expense> oldExpenses;
    private List<Expense> newExpenses;

    public ExpensesDiffUtilCallback(List<Expense> oldExpenses, List<Expense> newExpenses) {
        this.oldExpenses = oldExpenses;
        this.newExpenses = newExpenses;
    }

    @Override
    public int getOldListSize() {
        return oldExpenses.size();
    }

    @Override
    public int getNewListSize() {
        return newExpenses.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        Expense oldItem = oldExpenses.get(oldItemPosition);
        Expense newItem = newExpenses.get(newItemPosition);

        return oldItem.getId().equals(newItem.getId());
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return false;
    }
}
