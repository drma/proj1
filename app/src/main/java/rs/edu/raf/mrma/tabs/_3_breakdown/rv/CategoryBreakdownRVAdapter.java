package rs.edu.raf.mrma.tabs._3_breakdown.rv;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import rs.edu.raf.mrma.R;
import rs.edu.raf.mrma.model.Category;
import rs.edu.raf.mrma.tabs._3_breakdown.Colors;
import rs.edu.raf.mrma.tabs._4_add_category.rv.CategoriesRVAdapter;
import rs.edu.raf.mrma.tabs._4_add_category.rv.CategoriesRVDiffCallback;

public class CategoryBreakdownRVAdapter extends RecyclerView.Adapter<CategoryBreakdownViewHolder> {
    public CategoryBreakdownRVAdapter() {

    }

    private List<Category> categories = new ArrayList<>();

    public void setCategories(List<Category> categories) {
        CategoriesRVDiffCallback cb = new CategoriesRVDiffCallback(this.categories, categories);
        DiffUtil.DiffResult result = DiffUtil.calculateDiff(cb);
        this.categories.clear();
        this.categories.addAll(categories);
        result.dispatchUpdatesTo(this);
    }

    @NonNull
    @Override
    public CategoryBreakdownViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.category_breakdown_rv_card, parent, false);
        return new CategoryBreakdownViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryBreakdownViewHolder holder, int position) {
        holder.setContent(categories.get(position), Colors.COLORS[position]);
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }
}
