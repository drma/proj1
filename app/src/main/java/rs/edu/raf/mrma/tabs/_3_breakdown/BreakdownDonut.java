package rs.edu.raf.mrma.tabs._3_breakdown;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.text.TextUtils;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.util.Pair;

import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

import rs.edu.raf.mrma.R;
import rs.edu.raf.mrma.model.Category;
import rs.edu.raf.mrma.tabs._4_add_category.rv.CategoriesRVAdapter;

public class BreakdownDonut extends AppCompatTextView {
    private static float pxToDp(Context context, float px) {
        return px / context.getResources().getDisplayMetrics().density;
    }

    private static float dpToPx(Context context, float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }

    private static final float CIRCLE_STROKE_WIDTH_DP = 20;

    // Colors we use for background and foreground circle
    private int mForegroundCircleColor;
    private int mBackgroundCircleColor;
    private float mCircleStrokeWidthInPx;

    // Rectangle which holds dimensions for the circle
    private RectF mRectF;

    // Paint which we use to draw circles
    private Paint mPaint;

    public BreakdownDonut(Context context) {
        super(context);
        init(null);
    }

    public BreakdownDonut(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public BreakdownDonut(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);

        int length = Math.min(width, height);

        int newMeasureSpec = MeasureSpec.makeMeasureSpec(length, MeasureSpec.EXACTLY);

        super.onMeasure(newMeasureSpec, newMeasureSpec);
    }

    private void init(AttributeSet attrs) {
        parseAttributes(attrs);
        categoriesAndTotals = Collections.emptyList();
        mRectF = new RectF();
        mPaint = new Paint();
    }

    private List<Pair<Category, Double>> categoriesAndTotals;

    public void setCategories(List<Pair<Category, Double>> categoriesAndTotals) {
        this.categoriesAndTotals = categoriesAndTotals;
        this.invalidate();
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        float left = 0 + mCircleStrokeWidthInPx;
        float top = 0 + mCircleStrokeWidthInPx;
        float bottom = getHeight() - mCircleStrokeWidthInPx;
        float right = getWidth() - mCircleStrokeWidthInPx;

        mRectF.set(left, top, right, bottom);

        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(mCircleStrokeWidthInPx);
        mPaint.setStrokeCap(Paint.Cap.SQUARE);

        double total = calculateTotal();
        if (total == 0) {
            return;
        }
        int i;
        Category category;
        double totalForCategory;
        float arcStart = 0;
        float arcLength = 0;

        ListIterator<Pair<Category, Double>> iterator = categoriesAndTotals.listIterator();

        while (iterator.hasNext()) {
            i = iterator.nextIndex();
            Pair<Category, Double> categoryAndTotal = iterator.next();
            category = categoryAndTotal.first;
            totalForCategory = categoryAndTotal.second;

            // Cycle through the 30ish available colors
            mPaint.setColor(Color.parseColor(Colors.COLORS[i % Colors.COLORS.length]));

            arcLength = (float) ((totalForCategory/total) * 360);
            canvas.drawArc(mRectF, arcStart, arcLength, false, mPaint);

            arcStart += arcLength;
        }
    }

    private double calculateTotal() {
        return categoriesAndTotals.stream()
                .map(categoryAndTotal -> categoryAndTotal.second)
                .reduce((firstCategoryTotal, secondCategoryTotal) -> firstCategoryTotal + secondCategoryTotal)
                .orElse((double) 0);
    }

    private float getSweepAngle() {
        CharSequence text = getText();
        boolean isNumber = TextUtils.isDigitsOnly(text);
        boolean isEmpty = TextUtils.isEmpty(text);

        if (isEmpty || !isNumber){
            return 0;
        }

        int number = Integer.parseInt(text.toString());
        float angle = (number / 100f) * 360;

        return angle;
    }

    private void parseAttributes(AttributeSet attrs) {

        if (attrs == null) {
            return;
        }

        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.BreakdownDonut);
        mForegroundCircleColor = typedArray.getColor(R.styleable.BreakdownDonut_foregroundCircleColor, 0);
        mBackgroundCircleColor = typedArray.getColor(R.styleable.BreakdownDonut_backgroundCircleColor, 0);
        // If user didn't set circle stroke width in XML we want to pass default value
        // We defined it in DP, and we hava to cnvert it to pixels because Canvas works with pixels,
        // not density pixels (DP)
        int defaultValue = (int) dpToPx(getContext(), CIRCLE_STROKE_WIDTH_DP);
//        mCircleStrokeWidthInPx = typedArray.getDimensionPixelSize(R.styleable.BreakdownDonut_circleStrokeWidth, defaultValue);
        mCircleStrokeWidthInPx = defaultValue;
        typedArray.recycle();
    }
}
